#!/bin/bash

set -ex

pushd /mnt/mesa

# If a previous rebase failed for some reasons, make sure to abort it.
if [ -d .git/rebase-apply ]; then
    rm -rf .git/rebase-apply
fi

# Clean up the repo.
git clean -fdx .
git reset --hard
git checkout main

# Add the source remote if unknown.
set +e
remote=`git remote | grep -w $MESA_SOURCE_REMOTE`
set -ex
if [ -z $remote ]; then
    git remote add $MESA_SOURCE_REMOTE https://gitlab.freedesktop.org/$MESA_SOURCE_REMOTE/mesa.git
fi

# Fetch the source remote and checkout the branch.
git fetch $MESA_SOURCE_REMOTE

if [[ -z "${MESA_SOURCE_BRANCH}" ]]; then
    # Checkout the source commit.
    git checkout $MESA_SOURCE_COMMIT
else
    # Checkout the remote branch.
    git checkout $MESA_SOURCE_REMOTE/$MESA_SOURCE_BRANCH
fi

# 64-bit version
mkdir -p /mesa/install64
meson setup build-m64 \
    -D gallium-drivers= \
    -D vulkan-drivers=amd \
    -D gallium-va=disabled \
    -D gallium-nine=false \
    -D platforms=x11 \
    -D b_ndebug=true \
    --prefix /mesa/install64
ninja -C build-m64 install

# 32-bit version
mkdir -p /mesa/install32
meson setup build-m32 \
    -D gallium-drivers= \
    -D vulkan-drivers=amd \
    -D gallium-va=disabled \
    -D gallium-nine=false \
    -D platforms=x11 \
    -D b_ndebug=true \
    --prefix /mesa/install32 \
    --cross-file /mnt/testing/build-x86.build
ninja -C build-m32 install

rm -Rf build-m32
rm -Rf build-m64
popd
