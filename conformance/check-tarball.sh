#!/bin/bash

TMP_DIR=`pwd`/check_tarball

if [ $# != 1 ]; then
    echo "$0 <cts_tarball.tgz>"
    exit 1
fi

cts_tarball_path=$(realpath $1)
cts_tarball_name=$(basename $1)

set -ex

# Cleanup previous checks.
rm -rf $TMP_DIR

mkdir -p $TMP_DIR/tarball

# Grab the verify submission tool.
git clone https://github.com/KhronosGroup/VK-GL-CTS-Tools.git $TMP_DIR/VK-GL-CTS-Tools
pushd $TMP_DIR/VK-GL-CTS-Tools
git checkout e85b4d20779d48106a74a584b7a048df5147b8c7
popd

# Grab VK-GL-CTS which is used by the verify submission tool.
git clone https://github.com/KhronosGroup/VK-GL-CTS.git $TMP_DIR/VK-GL-CTS

# Run the verify submission tool.
cd $TMP_DIR/VK-GL-CTS-Tools
python verify_submission.py $cts_tarball_path -d $TMP_DIR/tarball -s $TMP_DIR/VK-GL-CTS -v
