#!/bin/bash

set -ex

SHADER_DB_VERSION="a0411a1a0da019d939ac2e8796aa72beed49cfce"

git clone https://gitlab.freedesktop.org/mesa/shader-db.git /shader-db
pushd /shader-db
git checkout $SHADER_DB_VERSION
cp fossil_replay.sh /usr/local/bin
cp report-fossil.py /usr/local/bin
popd
rm -rf /shader-db
