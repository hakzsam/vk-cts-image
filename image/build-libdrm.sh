#!/bin/bash

set -ex

LIBDRM_VERSION="libdrm-2.4.122"

git clone https://gitlab.freedesktop.org/mesa/drm.git /drm
pushd /drm
git checkout $LIBDRM_VERSION

meson build \
   -D amdgpu=enabled \
   -D radeon=enabled \
   -D intel=disabled \
   -D nouveau=disabled \
   -D vmwgfx=disabled
ninja -C build install

rm -rf /drm
popd
