FROM debian:testing

# Update and install the build software
RUN apt-get dist-upgrade -y
RUN apt-get update
RUN apt-get update # seems required to update correctly, wtf.

# TODO: Try to reduce the number of installed packages.
RUN apt-get install -y \
        bison \
        cargo \
        cmake \
        curl \
        flex \
        g++ \
        gcc \
        gettext \
        git \
        glslang-tools \
        libelf-dev \
        libexpat1-dev \
        libgles2-mesa-dev \
        libunwind8 \
        libvulkan-dev \
        libvulkan1 \
        libwaffle-dev \
        libx11-dev \
        libx11-xcb-dev \
        libxcb-ewmh-dev \
        libxcb-ewmh2 \
        libxcb-keysyms1 \
        libxcb-keysyms1-dev \
        libxcb-randr0 \
        libxcb-xfixes0 \
        libxdamage1 \
        libxfixes3 \
        libxfixes-dev \
        libxkbcommon-dev \
        libxkbcommon0 \
        libxrandr-dev \
        libxrandr2 \
        libxrender-dev \
        libxrender1 \
        libxshmfence-dev \
        libxxf86vm1 \
        libxxf86vm-dev \
        meson \
        ninja-build \
        pkg-config \
        python3 \
        python3-attr \
        python3-mako \
        python3-numpy \
        waffle-utils \
        wayland-protocols \
        wine64-tools \
        wget

# Dependencies where we want a specific version.
ARG XCB_RELEASES=https://xcb.freedesktop.org/dist
ARG XCBPROTO_VERSION=xcb-proto-1.15
ARG LIBXCB_VERSION=libxcb-1.15

RUN wget $XCB_RELEASES/$XCBPROTO_VERSION.tar.gz
RUN tar -xvf $XCBPROTO_VERSION.tar.gz && rm $XCBPROTO_VERSION.tar.gz
RUN cd $XCBPROTO_VERSION; ./configure; make -j4 install; cd ..
RUN rm -rf $XCBPROTO_VERSION

RUN wget $XCB_RELEASES/$LIBXCB_VERSION.tar.gz
RUN tar -xvf $LIBXCB_VERSION.tar.gz && rm $LIBXCB_VERSION.tar.gz
RUN cd $LIBXCB_VERSION; ./configure; make -j4 install; cd ..
RUN rm -rf $LIBXCB_VERSION

# Copy build scripts and patches.
COPY image/build-*.sh /root/
COPY image/*.diff /root/

# Ensure Git won't reject the repo.
RUN git config --global --add safe.directory /mnt/mesa

# Build dEQP.
RUN ./root/build-deqp.sh

# Build LLVM.
RUN ./root/build-llvm.sh

# Build libdrm.
RUN ./root/build-libdrm.sh

# Build deqp-runner.
RUN ./root/build-deqp-runner.sh

# Build Fossilize.
RUN ./root/build-fossilize.sh

# Build shader-db.
RUN ./root/build-shader-db.sh

# Build vkd3d-proton.
RUN ./root/build-vkd3d-proton.sh

# Build piglit.
RUN ./root/build-piglit.sh

# Remove build scripts and patches.
RUN rm ./root/build-*.sh
RUN rm ./root/*.diff

# TODO: Purge useless pacakges

RUN apt-get autoremove -y --purge
