#!/bin/bash

build_mesa()
{
    set -ex

    pushd /mnt/mesa

    # If a previous rebase failed for some reasons, make sure to abort it.
    if [ -d .git/rebase-apply ]; then
        rm -rf .git/rebase-apply
    fi

    # Clean up the repo.
    git clean -fdx .
    git reset --hard

    # Add the source remote if unknown.
    set +e
    remote=`git remote | grep -w $MESA_SOURCE_REMOTE`
    set -ex
    if [ -z $remote ]; then
        git remote add $MESA_SOURCE_REMOTE https://gitlab.freedesktop.org/$MESA_SOURCE_REMOTE/mesa.git
    fi

    # Fetch the source remote and checkout the commit or branch.
    if [[ -z "${MESA_SOURCE_BRANCH}" ]]; then
        git fetch $MESA_SOURCE_REMOTE $MESA_SOURCE_COMMIT
    else
        git fetch $MESA_SOURCE_REMOTE $MESA_SOURCE_BRANCH
        rm -f .git/refs/remotes/$MESA_SOURCE_REMOTE/$MESA_SOURCE_BRANCH
    fi
    git checkout FETCH_HEAD

    # Rebase the source branch on the origin/main branch if requested.
    if [ $MESA_REBASE_BRANCH -eq 1 ]; then
        if ! git pull origin main --rebase; then
            git rebase --abort
            exit 1
        fi
    fi

    # Build the branch.
    meson setup _build \
          -D prefix=`pwd`/install \
          -D libdir=lib \
          -D buildtype=$MESA_BUILDTYPE \
          -D platforms=x11 \
          -D gallium-drivers=${GALLIUM_DRIVERS:-} \
          -D vulkan-drivers=${VULKAN_DRIVERS:-} \
          -D build-aco-tests=$BUILD_ACO_TESTS \
          -D build-tests=false \
          -D werror=false \
          -D libunwind=disabled \
          -D tools=drm-shim \
          -D cpp_rtti=true

    cd _build

    ninja -j $NUM_THREADS
    LC_ALL=C.UTF-8 meson test --num-processes $NUM_THREADS
    ninja install
    cd ..

    popd
}

rebuild_mesa()
{
    set -ex
    pushd /mnt/mesa/_build
    git checkout $1
    ninja -j $NUM_THREADS
    LC_ALL=C.UTF-8 meson test --num-processes $NUM_THREADS
    ninja install
    popd
}
