#!/bin/bash

source /mnt/testing/build-mesa.sh
set -ex

RESULTS_DIR="/mnt/results"
output_files=()

get_output_file()
{
    # Get the sha1 of the current commit.
    pushd /mnt/mesa > /dev/null
    sha1=$(git rev-parse --short HEAD)
    popd > /dev/null

    remote=$(echo $MESA_SOURCE_REMOTE | tr '/' '-')
    branch=$(echo $MESA_SOURCE_BRANCH | tr '/' '-')

    # Build the statistics output filename.
    echo "$RESULTS_DIR/fossilize-$remote-$branch-$sha1.csv"
}

run_fossilize()
{
    output_file=$(get_output_file)

    export LD_LIBRARY_PATH=/mnt/mesa/install/lib/:/usr/local/lib:/usr/local/lib/x86_64-linux-gnu
    export VK_ICD_FILENAMES=/mnt/mesa/install/share/vulkan/icd.d/"$VK_DRIVER"_icd.x86_64.json

    if [ -n "$NUM_THREADS" ]; then
        FOSSILIZE_PARALLEL="--num-threads $NUM_THREADS"
    fi

    # Enable drm-shim and set AMDGPU_GPU_ID if a GPU family is forced.
    if [ -n "$GPU_FAMILY" ]; then
        export LD_PRELOAD=/mnt/mesa/install/lib/libamdgpu_noop_drm_shim.so
        export AMDGPU_GPU_ID="$GPU_FAMILY"
    fi

    /usr/local/bin/fossil_replay.sh /mnt/radv_fossils/fossils \
        $output_file $FOSSILIZE_PARALLEL

    if [ -n "$GPU_FAMILY" ]; then
        unset LD_PRELOAD
    fi

    # Add the output file to the list for later comparisons.
    output_files=($output_file ${output_files[@]})
}

# Run Fossilize against the source remote/branch.
build_mesa
run_fossilize

# Run fossilize against the last N commit (1 is the default which means it only
# test the top commit on the source remote/branch).
for (( i=1; i<$FOSSILIZE_LAST_N_COMMIT; i++ ))
do
    rebuild_mesa HEAD~
    run_fossilize
done

# If we want to report the Fossilize results, build the origin/main
# branch. This assumes that the source branch has been rebased first.
if [ $FOSSILIZE_COMPARE_MAIN -eq 1 ]; then
    rebuild_mesa origin/main
    run_fossilize
fi

set +x

# Report Fossilize results.
len=${#output_files[@]}
if [ $len > 1 ]; then
    for (( i=0; i<$len-1; i++ ))
    do
        before=${output_files[$i]}
        after=${output_files[$i+1]}

        set -ex
        /usr/local/bin/report-fossil.py $before $after
        set +x
    done
fi
