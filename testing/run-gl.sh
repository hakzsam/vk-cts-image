#!/bin/bash

source /mnt/testing/build-mesa.sh
set -ex

RESULTS_DIR="/mnt/results"

build_mesa
/mnt/testing/check-device.sh

pushd /mnt/mesa

set +e

export FDO_CI_CONCURRENT=$NUM_THREADS

# Copy the skips/fails/flakes lists.
cp .gitlab-ci/all-skips.txt install/
if [ "$GALLIUM_DRIVERS" == "zink" ]; then
    cp src/gallium/drivers/zink/ci/*radv* install/

    export GPU_VERSION="zink-radv-"$EXPECTED_GPU_FAMILY

    # FIXME: Use window surface type for vk-cts-image because VK-GL-CTS is built
    # with x11_glx instead of surfaceless for glesX.
    sed -i 's/pbuffer/window/g' install/deqp-zink-radv.toml
fi

export DEQP_RESULTS_DIR="../.."$RESULTS_DIR"/deqp"
/mnt/testing/deqp-runner.sh

popd
