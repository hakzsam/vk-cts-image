#!/bin/bash

TEST_IMAGE=$(cat IMAGE_VERSION)

result_dir=`pwd`/results
mesa_rebase_branch=1
mesa_build_type=debugoptimized
run_cts=1
run_vkd3d_proton=1

while getopts "hb:j:o:r:s:-:" OPTION; do
    case $OPTION in
    -)
        case "${OPTARG}" in
            no-rebase)
                mesa_rebase_branch=0
                ;;
            no-cts)
                run_cts=0
                ;;
            no-vkd3d-proton)
                run_vkd3d_proton=0
                ;;
            *)
                if [ "$OPTERR" = 1 ] && [ "${optspec:0:1}" != ":" ]; then
                    echo "Unknown option --${OPTARG}" >&2
                fi
                exit 0
                ;;
        esac;;
    h)
        echo -n "$0 -r <source_remote> -b <source_branch> "
        echo "[ -j <num_threads> -c <aco|llvm> -o <result_dir> --no-rebase ]"
        exit 0
        ;;
    b)
        mesa_branch=$OPTARG
        ;;
    j)
        num_threads=$OPTARG
        ;;
    o)
        result_dir=$OPTARG
        ;;
    r)
        mesa_remote=$OPTARG
        ;;
    s)
        mesa_commit=$OPTARG
        ;;
    *)
        if [ "$OPTERR" != 1 ] || [ "${optspec:0:1}" = ":" ]; then
            echo "Non-option argument: '-${OPTARG}'" >&2
        fi
        exit 1
        ;;
    esac
done

if [ -z $mesa_remote ] || ([ -z $mesa_branch ] && [ -z $mesa_commit ]); then
    echo "To use with a branch:"
    echo "$0 -r <mesa_remote> -b <mesa_branch> [-j <deqp_parallel>] [-d <result_dir>]"
    echo "To use with a commit:"
    echo "$0 -r <mesa_remote> -s <mesa_commit> [-j <deqp_parallel>] [-d <result_dir>]"
    exit 1
fi

if [ -z $num_threads ]; then
    num_threads=$(nproc)
fi

set -ex

mkdir -p $result_dir

# Pull the latest image.
docker pull $TEST_IMAGE

# Build a specific Mesa remote/branch and run CTS with deqp-runner.
docker run --rm \
    --mount src=`pwd`/testing,target=/mnt/testing,type=bind \
    --mount src=`pwd`/external/mesa,target=/mnt/mesa,type=bind \
    --mount src=$result_dir,target=/mnt/results,type=bind \
    --security-opt label=disable \
    --env MESA_SOURCE_REMOTE=$mesa_remote \
    --env MESA_SOURCE_BRANCH=$mesa_branch \
    --env MESA_SOURCE_COMMIT=$mesa_commit \
    --env MESA_REBASE_BRANCH=$mesa_rebase_branch \
    --env MESA_BUILDTYPE=$mesa_build_type \
    --env NUM_THREADS=$num_threads \
    --env RUN_CTS=$run_cts \
    --env RUN_VKD3D_PROTON=$run_vkd3d_proton \
    --env VKD3D_TEST_EXCLUDE="" \
    --env-file lvp-env \
    -it $TEST_IMAGE \
    bin/bash /mnt/testing/run-vk.sh
